#!/bin/bash -x

### configure apache2 for letsencrypt
mkdir -p /var/www/.well-known/acme-challenge/
cat <<EOF > /etc/apache2/conf-available/letsencrypt.conf
Alias /.well-known/acme-challenge /var/www/.well-known/acme-challenge
<Directory /var/www/.well-known/acme-challenge>
    Options None
    AllowOverride None
    ForceType text/plain
</Directory>
EOF
a2enconf letsencrypt
systemctl reload apache2
