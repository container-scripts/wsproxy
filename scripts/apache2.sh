#!/bin/bash -x

### redirect http-->https, except for letsencrypt requests
cat <<EOF > /etc/apache2/sites-available/000-default.conf
<VirtualHost *:80>
    RewriteEngine On
    RewriteCond %{REQUEST_URI} !^/.well-known/acme-challenge [NC]
    RewriteCond %{HTTPS} off
    RewriteRule ^ https://%{HTTP_HOST}%{REQUEST_URI}
</VirtualHost>
EOF
a2ensite 000-default

### apache2 modules
a2enmod ssl cache headers rewrite \
        proxy proxy_http proxy_wstunnel proxy_connect proxy_balancer

systemctl restart apache2

