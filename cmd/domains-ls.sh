cmd_domains-ls_help() {
    cat <<_EOF
    domains-ls [<pattern>]
         List the domains that match the given pattern.

_EOF
}

cmd_domains-ls() {
    ls sites-enabled/ \
        | grep .conf \
        | sed -e 's/.conf$//' \
        | sed -e '/000-default/d' \
        | grep -e "$1"
}
