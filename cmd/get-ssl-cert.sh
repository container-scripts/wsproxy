cmd_get-ssl-cert_help() {
    cat <<_EOF
    get-ssl-cert <email> <domain>... [-t,--test]
         Get free SSL certificates from letsencrypt.org

_EOF
}

cmd_get-ssl-cert() {
    local usage="Usage: $COMMAND <email> <domain>... [-t,--test]"

    # get the options and arguments
    local test=0
    local opts="$(getopt -o t -l test -- "$@")"
    local err=$?
    eval set -- "$opts"
    while true; do
        case $1 in
            -t|--test) test=1; shift ;;
            --) shift; break ;;
        esac
    done
    [[ $err == 0 ]] || fail $usage

    local email=$1 ; shift
    [[ -n $email ]] || fail $usage

    local domains="$@"
    [[ -n $domains ]] || fail $usage

    # build the certbot args
    local args="certonly  --non-interactive --webroot --keep-until-expiring"
    args+=" --email $email --agree-tos --webroot-path /var/www"
    local domain
    for domain in $domains; do
        args+=" -d $domain"
    done
    [[ $test == 1 ]] && args+=" --dry-run"

    # run certbot from inside the container
    cs exec certbot $args

    # if testing, stop here
    [[ $test == 1 ]] && exit 0

    # update config files
    local domain=$(echo $domains | cut -d' ' -f1)
    local certdir=/etc/letsencrypt/live/$domain
    [[ -f letsencrypt/live/$domain/cert.pem ]] || \
        fail "\nCan not find certificate files for: '$domains'.\nApache config not updated.\n"
    # update apache2 config file in wsproxy
    sed -i sites-available/$domain.conf -r \
        -e "s|#?SSLCertificateFile.*|SSLCertificateFile      $certdir/cert.pem|" \
        -e "s|#?SSLCertificateKeyFile.*|SSLCertificateKeyFile   $certdir/privkey.pem|" \
        -e "s|#?SSLCertificateChainFile.*|SSLCertificateChainFile $certdir/chain.pem|"

    # reload apache2 config
    cs exec systemctl reload apache2
}
