cmd_config_help() {
    cat <<_EOF
    config
        Configure the guest system inside the container.

_EOF
}

cmd_config() {
    cs inject ubuntu-fixes.sh
    cs inject ssmtp.sh
    cs inject logwatch.sh

    cs inject letsencrypt.sh
    cs inject apache2.sh
}
