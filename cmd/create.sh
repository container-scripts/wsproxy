cmd_create_help() {
    cat <<_EOF
    create
        Create the wsproxy container '$CONTAINER'.

_EOF
}

rename_function cmd_create orig_cmd_create
cmd_create() {
    mkdir -p sites-available sites-enabled letsencrypt fail2ban
    cp $APP_DIR/misc/apache2.conf sites-available/xmp.conf
    [[ -f fail2ban/jail.local ]] || cp $APP_DIR/misc/jail.local fail2ban/

    orig_cmd_create \
        --cap-add=NET_ADMIN \
        --mount type=bind,src=$(pwd)/letsencrypt,dst=/etc/letsencrypt \
        --mount type=bind,src=$(pwd)/sites-available,dst=/etc/apache2/sites-available \
        --mount type=bind,src=$(pwd)/sites-enabled,dst=/etc/apache2/sites-enabled \
        --mount type=bind,src=$(pwd)/fail2ban/jail.local,dst=/etc/fail2ban/jail.local

    _create_cmd_wsproxy
}

_create_cmd_wsproxy() {
    local wsproxydir=$(basename $(pwd))
    mkdir -p $CSDIR/cmd/
    local cmdfile=$CSDIR/cmd/wsproxy.sh
    cat <<-__EOF__ > $cmdfile
cmd_wsproxy_help() {
    cat <<_EOF
    wsproxy [ ls | add | rm | ssl-cert [<email>] ]
        Manage the domain '\$DOMAIN' of the container.

_EOF
}

cmd_wsproxy() {
    case \$1 in
        ls)        cs @$wsproxydir domains-ls   \$DOMAIN  ;;
        add)       cs @$wsproxydir domains-add  \$DOMAIN \$DOMAINS  ;;
        rm)        cs @$wsproxydir domains-rm   \$DOMAIN \$DOMAINS  ;;
        ssl-cert)
                   local email=\${2:-\${SSL_CERT_EMAIL:-\${ADMIN_EMAIL:-\$GMAIL_ADDRESS}}}
                   [[ -z \$email ]] && fail "Usage: cs wsproxy ssl-cert <email>"
                   local domain_list
                   for domain in \$DOMAIN \$DOMAINS; do
                       [[ \$domain =~ ^(.*\.)?example\.org\$ ]] && continue
                       [[ \$domain =~ ^(.*\.)?example\.com\$ ]] && continue
                       [[ \$domain =~ \.local\$ ]] && continue
                       domain_list+=" \$domain"
                   done
                   [[ -n \$domain_list ]] && cs @$wsproxydir get-ssl-cert \$email \$domain_list
                   ;;
        *)         echo -e "Usage:\\n\$(cmd_wsproxy_help)" ; exit ;;
    esac
}
__EOF__
}
