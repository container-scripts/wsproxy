cmd_domains-add_help() {
    cat <<_EOF
    domains-add <domain> [<domain> ...]
         Add one or more domains to the configuration of the web proxy.

_EOF
}

cmd_domains-add() {
    # get the domains
    [[ $# -lt 1 ]] && fail "Usage: $COMMAND <domain> [<domain> ...]"
    local domain=$1
    shift
    local aliases="$@"

    # remove these domains, if they exist
    cs domains-rm $domain $aliases

    # add an apache2 config file for the domain
    cp sites-available/{xmp.conf,$domain.conf}
    sed -i sites-available/$domain.conf \
        -e "s/example\.org/$domain/g"
    ln -s ../sites-available/$domain.conf sites-enabled/
    # add aliases on the apache2 config file
    [[ -n $aliases ]] && \
        sed -i sites-available/$domain.conf \
            -e "/ServerName/ a\        ServerAlias $aliases"

    # reload apache2 config
    cs exec systemctl reload apache2
}
