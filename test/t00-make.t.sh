source $(dirname $BASH_SOURCE)/common.sh

description 'Test make'

test_case 'cs make' '
    cs pull wsproxy &&
    rm_container_dir &&
    cs init wsproxy @wsproxy &&
    cd $CONTAINERS/wsproxy &&
    fix_settings &&
    cs make &&
    tail logs/nohup-*.out | grep "Successfully built" &&
    docker images --format "{{.Repository}}" | grep wsproxy-test &&
    docker ps -a --format "{{.Names}}" | grep wsproxy-test &&
    cs stop
'
