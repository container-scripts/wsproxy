include(focal)

RUN apt install --yes apache2 wget fail2ban net-tools

### install certbot (for getting ssl certs with letsencrypt)
RUN apt install --yes certbot
