# bash completions for the command `cs domains-add`
_cs_domains-add() {
    local containers=$(docker ps -a --format "{{.Names}}")
    COMPREPLY=( $(compgen -W "$containers" -- $1) )
}

# bash completions for the command `cs domains-rm`
_cs_domains-rm() {
    local domains=$(ls $(_cs_container_dir)/sites-enabled/ | grep .conf | sed -e 's/.conf$//' | sed -e '/000-default/d')
    COMPREPLY=( $(compgen -W "$domains" -- $1) )
}

# bash completions for the command `cs get-ssl-cert`
_cs_get-ssl-cert() {
    local domains=$(ls $(_cs_container_dir)/sites-enabled/ | grep .conf | sed -e 's/.conf$//' | sed -e '/000-default/d')
    COMPREPLY=( $(compgen -W "$domains" -- $1) )
}
