About Web Server Proxy
----------------------

If we want to host several domains/subdomains on the same webserver
we can use *name-based virtual hosting*. If we need to host these
domains/subdomains in different webservers, each one in its own
docker container, there is a problem because the ports 80/443 can
be used (exposed to the host) only by one of the containers.

In such a case the *Reverse Proxy* module of apache2 comes to the
rescue. We can have a docker container with apache2 that forwards
all the http requests to the other containers (webservers), behaving
like a kind of http gateway or hub. This is what **wsproxy** does.

Installation
------------

 + Install `cs` (container scripts):
   https://gitlab.com/container-scripts/cs#installation

 + Get the wsproxy scripts: `cs pull wsproxy`

 + Init a directory for the container: `cs init wsproxy @wsproxy`

 + Customize settings: `cd /var/cs/wsproxy/; vim settings.sh`

 + Make the container: `cs make`


Usage
-----

 + Create the containers of each webserver using commands like this:
 
   ```
   docker run -d --name=ws1 --hostname=test1.example.org webserver-1
   docker run -d --name=ws2 --hostname=test2.example.org webserver-2
   ```
   
   Note that HTTP ports are NOT exposed to the host (for example using
   options `-p 80:80 -p 443:443`).

 + Add domains for `test1.example.org` and `test2.example.org`:
 
   ```
   cs domains-add ws1 test1.example.org
   cs domains-add ws2 test2.example.org test3.example.org
   ```

 + Get a letsencrypt.org free SSL cert for the domains:

   ```
   cs get-ssl-cert info@test1.example.org test1.example.org test2.example.org --test
   cs get-ssl-cert info@test1.example.org test1.example.org test2.example.org
   ```


Commands
--------

    domains-add <container> <domain> <domain> ...
         Add one or more domains to the configuration of the web proxy.

    domains-rm <domain> <domain> ...
         Remove one or more domains from the configuration of the web proxy.

    get-ssl-cert <email> <domain>... [-t,--test]
         Get free SSL certificates from letsencrypt.org


How it works
------------

HTTPS requests are forwarded to another webserver/container with a
configuration like this:

```
<VirtualHost _default_:443>
    ServerName example.org
    ProxyPreserveHost on
    ProxyPass / https://example.org/
    ProxyPassReverse / https://example.org/

    ProxyRequests off

    SSLEngine on
    SSLCertificateFile     /etc/ssl/certs/ssl-cert-snakeoil.pem
    SSLCertificateKeyFile  /etc/ssl/private/ssl-cert-snakeoil.key
    #SSLCertificateChainFile /etc/ssl/certs/ssl-cert-snakeoil.pem

    SSLProxyEngine on
    SSLProxyVerify none
    SSLProxyCheckPeerCN off
    SSLProxyCheckPeerName off
</VirtualHost>
```

These apache2 modules are enabled:

```
a2enmod ssl cache headers rewrite \
        proxy proxy_http proxy_wstunnel proxy_connect proxy_balancer
```
